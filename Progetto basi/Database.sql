USE [ProgettoBasi]
GO
/****** Object:  Table [dbo].[ACCOUNT]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ACCOUNT](
	[Utente] [char](15) NOT NULL,
	[Password] [char](15) NOT NULL,
	[Nome] [char](15) NOT NULL,
	[Cognome] [char](15) NOT NULL,
	[CodFisc] [char](16) NOT NULL,
	[NumTel] [numeric](15, 0) NOT NULL,
	[Direttore] [char](1) NOT NULL,
 CONSTRAINT [IDACCOUNT_ID] PRIMARY KEY CLUSTERED 
(
	[Utente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CATEGORIE]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CATEGORIE](
	[IdCategoria] [numeric](5, 0) NOT NULL,
	[Nome] [char](50) NULL,
	[Descrizione] [char](200) NOT NULL,
 CONSTRAINT [IDCATEGORIE_ID] PRIMARY KEY CLUSTERED 
(
	[IdCategoria] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Comprendono]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Comprendono](
	[IdProdotto] [numeric](5, 0) NOT NULL,
	[IdFornitura] [numeric](5, 0) NOT NULL,
	[Quantità] [numeric](5, 0) NOT NULL,
	[Prezzo] [numeric](5, 2) NOT NULL,
 CONSTRAINT [IDComprendono] PRIMARY KEY CLUSTERED 
(
	[IdFornitura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CORSIE]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CORSIE](
	[IdSupermercato] [numeric](5, 0) NOT NULL,
	[NumCorsia] [numeric](2, 0) NOT NULL,
	[Capacità] [numeric](5, 0) NOT NULL,
	[IdCategoria] [numeric](5, 0) NOT NULL,
 CONSTRAINT [IDCORSIE_SUPERMERCATO_ID] PRIMARY KEY CLUSTERED 
(
	[IdSupermercato] ASC,
	[NumCorsia] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Di]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Di](
	[NumScontrino] [numeric](5, 0) NOT NULL,
	[IdProdotto] [numeric](5, 0) NOT NULL,
	[Data] [date] NOT NULL,
	[Quantità] [numeric](5, 0) NOT NULL,
 CONSTRAINT [IDDi] PRIMARY KEY CLUSTERED 
(
	[NumScontrino] ASC,
	[IdProdotto] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Forniscono]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Forniscono](
	[IdProdotto] [numeric](5, 0) NOT NULL,
	[IdRifornitore] [numeric](5, 0) NOT NULL,
 CONSTRAINT [IDForniscono] PRIMARY KEY CLUSTERED 
(
	[IdRifornitore] ASC,
	[IdProdotto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[FORNITURE]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FORNITURE](
	[IdFornitura] [numeric](5, 0) NOT NULL,
	[Data] [date] NOT NULL,
	[IdSupermercato] [numeric](5, 0) NOT NULL,
	[IdRifornitore] [numeric](5, 0) NOT NULL,
 CONSTRAINT [IDFORNITURE_ID] PRIMARY KEY CLUSTERED 
(
	[IdFornitura] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OFFERTE]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OFFERTE](
	[IdOfferta] [numeric](5, 0) NOT NULL,
	[DataInzio] [date] NOT NULL,
	[DataFine] [date] NOT NULL,
	[Descrizione] [char](200) NULL,
	[PercentualeSconto] [numeric](3, 0) NOT NULL,
 CONSTRAINT [IDOFFERTE_ID] PRIMARY KEY CLUSTERED 
(
	[IdOfferta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PREZZI_ATTUALI]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PREZZI_ATTUALI](
	[IdProdotto] [numeric](5, 0) NOT NULL,
	[Data] [date] NOT NULL,
	[Valore] [numeric](5, 2) NOT NULL,
 CONSTRAINT [IDPREZZI_ATTUALI] PRIMARY KEY CLUSTERED 
(
	[IdProdotto] ASC,
	[Data] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PRODOTTI]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PRODOTTI](
	[IdProdotto] [numeric](5, 0) NOT NULL,
	[Nome] [char](50) NOT NULL,
	[Descrizione] [char](200) NOT NULL,
	[PrezzoUnitario] [numeric](5, 2) NOT NULL,
	[Data_d_entrata_in_commercio] [date] NOT NULL,
	[IdCategoria] [numeric](5, 0) NOT NULL,
	[IdOfferta] [numeric](5, 0) NULL,
 CONSTRAINT [IDPRODOTTI_ID] PRIMARY KEY CLUSTERED 
(
	[IdProdotto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[RIFORNITORI]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RIFORNITORI](
	[IdRifornitore] [numeric](5, 0) NOT NULL,
	[Nome] [char](15) NOT NULL,
	[Località] [char](15) NOT NULL,
	[Ind_ViaPiazza] [char](15) NOT NULL,
	[Ind_NumCivico] [numeric](5, 0) NOT NULL,
 CONSTRAINT [IDRIFORNITORI_ID] PRIMARY KEY CLUSTERED 
(
	[IdRifornitore] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SCONTRINI]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SCONTRINI](
	[NumScontrino] [numeric](5, 0) NOT NULL,
	[Data] [date] NOT NULL,
	[Totale] [numeric](5, 2) NOT NULL,
	[IdSupermercato] [numeric](5, 0) NOT NULL,
 CONSTRAINT [IDSCONTRINI_ID] PRIMARY KEY CLUSTERED 
(
	[NumScontrino] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SUPERMERCATI]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SUPERMERCATI](
	[IdSupermercato] [numeric](5, 0) NOT NULL,
	[Utente] [char](15) NOT NULL,
	[Nome] [char](15) NOT NULL,
	[Località] [char](15) NOT NULL,
	[Ind_ViaPiazza] [char](15) NOT NULL,
	[Ind_NumCivico] [numeric](5, 0) NOT NULL,
	[Guadagno_netto] [numeric](5, 2) NOT NULL,
 CONSTRAINT [IDSEDI_ID] PRIMARY KEY CLUSTERED 
(
	[IdSupermercato] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Tengono]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tengono](
	[IdSupermercato] [numeric](5, 0) NOT NULL,
	[NumCorsia] [numeric](2, 0) NOT NULL,
	[IdProdotto] [numeric](5, 0) NOT NULL,
	[Quantità] [numeric](2, 0) NOT NULL,
 CONSTRAINT [IDTengono] PRIMARY KEY CLUSTERED 
(
	[IdSupermercato] ASC,
	[NumCorsia] ASC,
	[IdProdotto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TURNI]    Script Date: 14/07/2016 10:28:32 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TURNI](
	[IdSupermercato] [numeric](5, 0) NOT NULL,
	[NumCorsia] [numeric](2, 0) NOT NULL,
	[OraInizio] [numeric](4, 2) NOT NULL,
	[OraFine] [numeric](4, 2) NOT NULL,
	[Utente] [char](15) NOT NULL,
 CONSTRAINT [IDTURNI] PRIMARY KEY CLUSTERED 
(
	[IdSupermercato] ASC,
	[NumCorsia] ASC,
	[OraInizio] ASC,
	[OraFine] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
INSERT [dbo].[ACCOUNT] ([Utente], [Password], [Nome], [Cognome], [CodFisc], [NumTel], [Direttore]) VALUES (N'admin          ', N'admin          ', N'Filippo        ', N'Ghinelli       ', N'asdf            ', CAST(0 AS Numeric(15, 0)), N'1')
INSERT [dbo].[ACCOUNT] ([Utente], [Password], [Nome], [Cognome], [CodFisc], [NumTel], [Direttore]) VALUES (N'director1      ', N'director1      ', N'Giordano       ', N'Selva          ', N'gdias           ', CAST(4567 AS Numeric(15, 0)), N'1')
INSERT [dbo].[ACCOUNT] ([Utente], [Password], [Nome], [Cognome], [CodFisc], [NumTel], [Direttore]) VALUES (N'director2      ', N'director2      ', N'Lucia          ', N'Rogna          ', N'weior           ', CAST(78910 AS Numeric(15, 0)), N'1')
INSERT [dbo].[ACCOUNT] ([Utente], [Password], [Nome], [Cognome], [CodFisc], [NumTel], [Direttore]) VALUES (N'employee       ', N'employee       ', N'Valerio        ', N'Rovi           ', N'asdfg           ', CAST(1234 AS Numeric(15, 0)), N'0')
INSERT [dbo].[ACCOUNT] ([Utente], [Password], [Nome], [Cognome], [CodFisc], [NumTel], [Direttore]) VALUES (N'employee1      ', N'employee1      ', N'Silvia         ', N'Trattori       ', N'nbewi           ', CAST(7315 AS Numeric(15, 0)), N'0')
INSERT [dbo].[ACCOUNT] ([Utente], [Password], [Nome], [Cognome], [CodFisc], [NumTel], [Direttore]) VALUES (N'employee2      ', N'employee2      ', N'Mario          ', N'Bevilacqua     ', N'fwfwi           ', CAST(3462 AS Numeric(15, 0)), N'0')
INSERT [dbo].[ACCOUNT] ([Utente], [Password], [Nome], [Cognome], [CodFisc], [NumTel], [Direttore]) VALUES (N'employee3      ', N'employee3      ', N'prova nome     ', N'prova cognome  ', N'abc             ', CAST(123456789 AS Numeric(15, 0)), N'0')
INSERT [dbo].[CATEGORIE] ([IdCategoria], [Nome], [Descrizione]) VALUES (CAST(1 AS Numeric(5, 0)), N'Carne fresca                                      ', N'Tutti i prodotti di carne fresca                                                                                                                                                                        ')
INSERT [dbo].[CATEGORIE] ([IdCategoria], [Nome], [Descrizione]) VALUES (CAST(2 AS Numeric(5, 0)), N'Pane e dolci freschi                              ', N'Pane e dolci fatti in giornata                                                                                                                                                                          ')
INSERT [dbo].[CATEGORIE] ([IdCategoria], [Nome], [Descrizione]) VALUES (CAST(3 AS Numeric(5, 0)), N'Snack e bibite                                    ', N'Tutto i prodotti salati considerati snack e bibite                                                                                                                                                      ')
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)), CAST(10 AS Numeric(5, 0)), CAST(5.00 AS Numeric(5, 2)))
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)), CAST(100 AS Numeric(5, 0)), CAST(70.00 AS Numeric(5, 2)))
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(9 AS Numeric(5, 0)), CAST(3 AS Numeric(5, 0)), CAST(100 AS Numeric(5, 0)), CAST(100.00 AS Numeric(5, 2)))
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(5 AS Numeric(5, 0)), CAST(4 AS Numeric(5, 0)), CAST(10 AS Numeric(5, 0)), CAST(20.00 AS Numeric(5, 2)))
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(7 AS Numeric(5, 0)), CAST(3 AS Numeric(5, 0)), CAST(20.00 AS Numeric(5, 2)))
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(7 AS Numeric(5, 0)), CAST(9 AS Numeric(5, 0)), CAST(100 AS Numeric(5, 0)), CAST(18.50 AS Numeric(5, 2)))
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(8 AS Numeric(5, 0)), CAST(44 AS Numeric(5, 0)), CAST(13 AS Numeric(5, 0)), CAST(6.50 AS Numeric(5, 2)))
INSERT [dbo].[Comprendono] ([IdProdotto], [IdFornitura], [Quantità], [Prezzo]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(77 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)), CAST(8.50 AS Numeric(5, 2)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(100 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(100 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(100 AS Numeric(5, 0)), CAST(3 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(200 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(200 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(200 AS Numeric(5, 0)), CAST(3 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(10 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(10 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[CORSIE] ([IdSupermercato], [NumCorsia], [Capacità], [IdCategoria]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(10 AS Numeric(5, 0)), CAST(3 AS Numeric(5, 0)))
INSERT [dbo].[Di] ([NumScontrino], [IdProdotto], [Data], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(3 AS Numeric(5, 0)))
INSERT [dbo].[Di] ([NumScontrino], [IdProdotto], [Data], [Quantità]) VALUES (CAST(99 AS Numeric(5, 0)), CAST(4 AS Numeric(5, 0)), CAST(N'2016-06-30' AS Date), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Di] ([NumScontrino], [IdProdotto], [Data], [Quantità]) VALUES (CAST(100 AS Numeric(5, 0)), CAST(6 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Di] ([NumScontrino], [IdProdotto], [Data], [Quantità]) VALUES (CAST(100 AS Numeric(5, 0)), CAST(7 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Di] ([NumScontrino], [IdProdotto], [Data], [Quantità]) VALUES (CAST(101 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Di] ([NumScontrino], [IdProdotto], [Data], [Quantità]) VALUES (CAST(101 AS Numeric(5, 0)), CAST(10 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Di] ([NumScontrino], [IdProdotto], [Data], [Quantità]) VALUES (CAST(102 AS Numeric(5, 0)), CAST(10 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(10 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(4 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(5 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(6 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(7 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(8 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(9 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(10 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(8 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(9 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[Forniscono] ([IdProdotto], [IdRifornitore]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(3 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(N'2016-06-30' AS Date), CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(4 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(7 AS Numeric(5, 0)), CAST(N'2016-07-11' AS Date), CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(9 AS Numeric(5, 0)), CAST(N'2016-07-11' AS Date), CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(44 AS Numeric(5, 0)), CAST(N'2016-07-11' AS Date), CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[FORNITURE] ([IdFornitura], [Data], [IdSupermercato], [IdRifornitore]) VALUES (CAST(77 AS Numeric(5, 0)), CAST(N'2016-07-11' AS Date), CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[OFFERTE] ([IdOfferta], [DataInzio], [DataFine], [Descrizione], [PercentualeSconto]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(N'2016-06-30' AS Date), CAST(N'2016-09-30' AS Date), N'Sconto sugli snack                                                                                                                                                                                      ', CAST(20 AS Numeric(3, 0)))
INSERT [dbo].[OFFERTE] ([IdOfferta], [DataInzio], [DataFine], [Descrizione], [PercentualeSconto]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(N'2016-09-30' AS Date), N'Sconto sul pane e dolci freschi                                                                                                                                                                         ', CAST(15 AS Numeric(3, 0)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(N'2016-06-30' AS Date), CAST(0.90 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(4.00 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(N'2016-06-30' AS Date), CAST(1.36 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(4 AS Numeric(5, 0)), CAST(N'2016-06-30' AS Date), CAST(2.13 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(5 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(3.40 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(6 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(3.70 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(7 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(3.70 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(8 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1.04 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(9 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1.67 AS Numeric(5, 2)))
INSERT [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data], [Valore]) VALUES (CAST(10 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1.70 AS Numeric(5, 2)))
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(1 AS Numeric(5, 0)), N'Pane 00                                           ', N'Pane fatto in giornata con farina 00                                                                                                                                                                    ', CAST(1.05 AS Numeric(5, 2)), CAST(N'2016-06-30' AS Date), CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(2 AS Numeric(5, 0)), N'Petto di pollo                                    ', N'Petto di pollo fresco                                                                                                                                                                                   ', CAST(4.00 AS Numeric(5, 2)), CAST(N'2016-06-30' AS Date), CAST(1 AS Numeric(5, 0)), NULL)
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(3 AS Numeric(5, 0)), N'Patatine alla paprika                             ', N'Pacco di patatine alla paprika                                                                                                                                                                          ', CAST(1.70 AS Numeric(5, 2)), CAST(N'2016-06-30' AS Date), CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(4 AS Numeric(5, 0)), N'Pizzette                                          ', N'Piccole pizze fatte in giornata                                                                                                                                                                         ', CAST(2.50 AS Numeric(5, 2)), CAST(N'2016-07-01' AS Date), CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(5 AS Numeric(5, 0)), N'Pasticcini vari                                   ', N'Pasticcini di diversi tipi fatti in giornata                                                                                                                                                            ', CAST(4.00 AS Numeric(5, 2)), CAST(N'2016-07-01' AS Date), CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(6 AS Numeric(5, 0)), N'Salsicce                                          ', N'Set di 3 salsicce fresche                                                                                                                                                                               ', CAST(3.70 AS Numeric(5, 2)), CAST(N'2016-07-01' AS Date), CAST(1 AS Numeric(5, 0)), NULL)
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(7 AS Numeric(5, 0)), N'Hamburger                                         ', N'Set di 2 hamburger di cane macinata fresca                                                                                                                                                              ', CAST(3.70 AS Numeric(5, 2)), CAST(N'2016-07-01' AS Date), CAST(1 AS Numeric(5, 0)), NULL)
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(8 AS Numeric(5, 0)), N'Arachidi salate                                   ', N'Arachidi tostate e condite                                                                                                                                                                              ', CAST(1.30 AS Numeric(5, 2)), CAST(N'2016-07-01' AS Date), CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(9 AS Numeric(5, 0)), N'Pringles cheddar cheese                           ', N'Patatine aromatizzate al gusto cheddar                                                                                                                                                                  ', CAST(2.09 AS Numeric(5, 2)), CAST(N'2016-07-01' AS Date), CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[PRODOTTI] ([IdProdotto], [Nome], [Descrizione], [PrezzoUnitario], [Data_d_entrata_in_commercio], [IdCategoria], [IdOfferta]) VALUES (CAST(10 AS Numeric(5, 0)), N'Spianata con olive                                ', N'Spianata fresca con olive fatta in giornata                                                                                                                                                             ', CAST(2.00 AS Numeric(5, 2)), CAST(N'2016-07-01' AS Date), CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[RIFORNITORI] ([IdRifornitore], [Nome], [Località], [Ind_ViaPiazza], [Ind_NumCivico]) VALUES (CAST(1 AS Numeric(5, 0)), N'ProvaRifor     ', N'Altrove        ', N'Via sconosciuta', CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[RIFORNITORI] ([IdRifornitore], [Nome], [Località], [Ind_ViaPiazza], [Ind_NumCivico]) VALUES (CAST(2 AS Numeric(5, 0)), N'Rifornitore1   ', N'Altrove        ', N'Via Incognita  ', CAST(100 AS Numeric(5, 0)))
INSERT [dbo].[RIFORNITORI] ([IdRifornitore], [Nome], [Località], [Ind_ViaPiazza], [Ind_NumCivico]) VALUES (CAST(3 AS Numeric(5, 0)), N'Rifornitore2   ', N'Altrove        ', N'Via Scomparsa  ', CAST(5 AS Numeric(5, 0)))
INSERT [dbo].[SCONTRINI] ([NumScontrino], [Data], [Totale], [IdSupermercato]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(12.00 AS Numeric(5, 2)), CAST(1 AS Numeric(5, 0)))
INSERT [dbo].[SCONTRINI] ([NumScontrino], [Data], [Totale], [IdSupermercato]) VALUES (CAST(99 AS Numeric(5, 0)), CAST(N'2016-06-30' AS Date), CAST(2.13 AS Numeric(5, 2)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[SCONTRINI] ([NumScontrino], [Data], [Totale], [IdSupermercato]) VALUES (CAST(100 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(7.40 AS Numeric(5, 2)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[SCONTRINI] ([NumScontrino], [Data], [Totale], [IdSupermercato]) VALUES (CAST(101 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(5.70 AS Numeric(5, 2)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[SCONTRINI] ([NumScontrino], [Data], [Totale], [IdSupermercato]) VALUES (CAST(102 AS Numeric(5, 0)), CAST(N'2016-07-01' AS Date), CAST(1.70 AS Numeric(5, 2)), CAST(2 AS Numeric(5, 0)))
INSERT [dbo].[SUPERMERCATI] ([IdSupermercato], [Utente], [Nome], [Località], [Ind_ViaPiazza], [Ind_NumCivico], [Guadagno_netto]) VALUES (CAST(1 AS Numeric(5, 0)), N'admin          ', N'ProvaSuper     ', N'Altrove        ', N'Via Sconosciuta', CAST(1 AS Numeric(5, 0)), CAST(7.00 AS Numeric(5, 2)))
INSERT [dbo].[SUPERMERCATI] ([IdSupermercato], [Utente], [Nome], [Località], [Ind_ViaPiazza], [Ind_NumCivico], [Guadagno_netto]) VALUES (CAST(2 AS Numeric(5, 0)), N'director1      ', N'Supermercato1  ', N'Altrove        ', N'Via Incognita  ', CAST(7 AS Numeric(5, 0)), CAST(-208.07 AS Numeric(5, 2)))
INSERT [dbo].[SUPERMERCATI] ([IdSupermercato], [Utente], [Nome], [Località], [Ind_ViaPiazza], [Ind_NumCivico], [Guadagno_netto]) VALUES (CAST(3 AS Numeric(5, 0)), N'director2      ', N'Supermercato2  ', N'Altrove        ', N'Via Scoperta   ', CAST(28 AS Numeric(5, 0)), CAST(-18.50 AS Numeric(5, 2)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(2 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(6 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(7 AS Numeric(5, 0)), CAST(20 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(1 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(4 AS Numeric(5, 0)), CAST(15 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(5 AS Numeric(5, 0)), CAST(5 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(10 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(3 AS Numeric(5, 0)), CAST(15 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(8 AS Numeric(5, 0)), CAST(15 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(9 AS Numeric(5, 0)), CAST(15 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(2 AS Numeric(5, 0)), CAST(5 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(6 AS Numeric(5, 0)), CAST(5 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(7 AS Numeric(5, 0)), CAST(5 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(1 AS Numeric(5, 0)), CAST(5 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(4 AS Numeric(5, 0)), CAST(5 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(5 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(8 AS Numeric(5, 0)), CAST(13 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(9 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(3 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(8 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[Tengono] ([IdSupermercato], [NumCorsia], [IdProdotto], [Quantità]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(9 AS Numeric(5, 0)), CAST(10 AS Numeric(2, 0)))
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(9.00 AS Numeric(4, 2)), CAST(12.00 AS Numeric(4, 2)), N'employee       ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(1 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(14.00 AS Numeric(4, 2)), CAST(17.00 AS Numeric(4, 2)), N'employee       ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(11.00 AS Numeric(4, 2)), CAST(13.00 AS Numeric(4, 2)), N'employee2      ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(2 AS Numeric(5, 0)), CAST(3 AS Numeric(2, 0)), CAST(17.00 AS Numeric(4, 2)), CAST(20.00 AS Numeric(4, 2)), N'employee2      ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(1 AS Numeric(2, 0)), CAST(11.00 AS Numeric(4, 2)), CAST(13.00 AS Numeric(4, 2)), N'employee1      ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(7.00 AS Numeric(4, 2)), CAST(9.00 AS Numeric(4, 2)), N'employee3      ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(9.00 AS Numeric(4, 2)), CAST(10.00 AS Numeric(4, 2)), N'employee3      ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(10.00 AS Numeric(4, 2)), CAST(11.00 AS Numeric(4, 2)), N'employee3      ')
INSERT [dbo].[TURNI] ([IdSupermercato], [NumCorsia], [OraInizio], [OraFine], [Utente]) VALUES (CAST(3 AS Numeric(5, 0)), CAST(2 AS Numeric(2, 0)), CAST(14.00 AS Numeric(4, 2)), CAST(15.00 AS Numeric(4, 2)), N'employee3      ')
SET ANSI_PADDING ON

GO
/****** Object:  Index [FKGestiscono_ID]    Script Date: 14/07/2016 10:28:32 ******/
ALTER TABLE [dbo].[SUPERMERCATI] ADD  CONSTRAINT [FKGestiscono_ID] UNIQUE NONCLUSTERED 
(
	[Utente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Comprendono]  WITH CHECK ADD  CONSTRAINT [FKCom_FOR] FOREIGN KEY([IdFornitura])
REFERENCES [dbo].[FORNITURE] ([IdFornitura])
GO
ALTER TABLE [dbo].[Comprendono] CHECK CONSTRAINT [FKCom_FOR]
GO
ALTER TABLE [dbo].[Comprendono]  WITH CHECK ADD  CONSTRAINT [FKCom_PRO] FOREIGN KEY([IdProdotto])
REFERENCES [dbo].[PRODOTTI] ([IdProdotto])
GO
ALTER TABLE [dbo].[Comprendono] CHECK CONSTRAINT [FKCom_PRO]
GO
ALTER TABLE [dbo].[CORSIE]  WITH CHECK ADD  CONSTRAINT [FKComposti_da] FOREIGN KEY([IdSupermercato])
REFERENCES [dbo].[SUPERMERCATI] ([IdSupermercato])
GO
ALTER TABLE [dbo].[CORSIE] CHECK CONSTRAINT [FKComposti_da]
GO
ALTER TABLE [dbo].[CORSIE]  WITH CHECK ADD  CONSTRAINT [FKContengono] FOREIGN KEY([IdCategoria])
REFERENCES [dbo].[CATEGORIE] ([IdCategoria])
GO
ALTER TABLE [dbo].[CORSIE] CHECK CONSTRAINT [FKContengono]
GO
ALTER TABLE [dbo].[Di]  WITH CHECK ADD  CONSTRAINT [FKDi_PRE] FOREIGN KEY([IdProdotto], [Data])
REFERENCES [dbo].[PREZZI_ATTUALI] ([IdProdotto], [Data])
ON UPDATE CASCADE
GO
ALTER TABLE [dbo].[Di] CHECK CONSTRAINT [FKDi_PRE]
GO
ALTER TABLE [dbo].[Di]  WITH CHECK ADD  CONSTRAINT [FKDi_SCO] FOREIGN KEY([NumScontrino])
REFERENCES [dbo].[SCONTRINI] ([NumScontrino])
GO
ALTER TABLE [dbo].[Di] CHECK CONSTRAINT [FKDi_SCO]
GO
ALTER TABLE [dbo].[Forniscono]  WITH CHECK ADD  CONSTRAINT [FKFor_PRO] FOREIGN KEY([IdProdotto])
REFERENCES [dbo].[PRODOTTI] ([IdProdotto])
GO
ALTER TABLE [dbo].[Forniscono] CHECK CONSTRAINT [FKFor_PRO]
GO
ALTER TABLE [dbo].[Forniscono]  WITH CHECK ADD  CONSTRAINT [FKFor_RIF] FOREIGN KEY([IdRifornitore])
REFERENCES [dbo].[RIFORNITORI] ([IdRifornitore])
GO
ALTER TABLE [dbo].[Forniscono] CHECK CONSTRAINT [FKFor_RIF]
GO
ALTER TABLE [dbo].[FORNITURE]  WITH CHECK ADD  CONSTRAINT [FKOrdinano] FOREIGN KEY([IdSupermercato])
REFERENCES [dbo].[SUPERMERCATI] ([IdSupermercato])
GO
ALTER TABLE [dbo].[FORNITURE] CHECK CONSTRAINT [FKOrdinano]
GO
ALTER TABLE [dbo].[FORNITURE]  WITH CHECK ADD  CONSTRAINT [FKProvvedono] FOREIGN KEY([IdRifornitore])
REFERENCES [dbo].[RIFORNITORI] ([IdRifornitore])
GO
ALTER TABLE [dbo].[FORNITURE] CHECK CONSTRAINT [FKProvvedono]
GO
ALTER TABLE [dbo].[PREZZI_ATTUALI]  WITH CHECK ADD  CONSTRAINT [FKPresentano] FOREIGN KEY([IdProdotto])
REFERENCES [dbo].[PRODOTTI] ([IdProdotto])
GO
ALTER TABLE [dbo].[PREZZI_ATTUALI] CHECK CONSTRAINT [FKPresentano]
GO
ALTER TABLE [dbo].[PRODOTTI]  WITH CHECK ADD  CONSTRAINT [FKA_cui_appartengono] FOREIGN KEY([IdCategoria])
REFERENCES [dbo].[CATEGORIE] ([IdCategoria])
GO
ALTER TABLE [dbo].[PRODOTTI] CHECK CONSTRAINT [FKA_cui_appartengono]
GO
ALTER TABLE [dbo].[PRODOTTI]  WITH CHECK ADD  CONSTRAINT [FKRiguardano] FOREIGN KEY([IdOfferta])
REFERENCES [dbo].[OFFERTE] ([IdOfferta])
GO
ALTER TABLE [dbo].[PRODOTTI] CHECK CONSTRAINT [FKRiguardano]
GO
ALTER TABLE [dbo].[SCONTRINI]  WITH CHECK ADD  CONSTRAINT [FKRegistrano] FOREIGN KEY([IdSupermercato])
REFERENCES [dbo].[SUPERMERCATI] ([IdSupermercato])
GO
ALTER TABLE [dbo].[SCONTRINI] CHECK CONSTRAINT [FKRegistrano]
GO
ALTER TABLE [dbo].[SUPERMERCATI]  WITH CHECK ADD  CONSTRAINT [FKGestiscono_FK] FOREIGN KEY([Utente])
REFERENCES [dbo].[ACCOUNT] ([Utente])
GO
ALTER TABLE [dbo].[SUPERMERCATI] CHECK CONSTRAINT [FKGestiscono_FK]
GO
ALTER TABLE [dbo].[Tengono]  WITH CHECK ADD  CONSTRAINT [FKTen_COR] FOREIGN KEY([IdSupermercato], [NumCorsia])
REFERENCES [dbo].[CORSIE] ([IdSupermercato], [NumCorsia])
GO
ALTER TABLE [dbo].[Tengono] CHECK CONSTRAINT [FKTen_COR]
GO
ALTER TABLE [dbo].[Tengono]  WITH CHECK ADD  CONSTRAINT [FKTen_PRO] FOREIGN KEY([IdProdotto])
REFERENCES [dbo].[PRODOTTI] ([IdProdotto])
GO
ALTER TABLE [dbo].[Tengono] CHECK CONSTRAINT [FKTen_PRO]
GO
ALTER TABLE [dbo].[TURNI]  WITH CHECK ADD  CONSTRAINT [FKImpegnati] FOREIGN KEY([Utente])
REFERENCES [dbo].[ACCOUNT] ([Utente])
GO
ALTER TABLE [dbo].[TURNI] CHECK CONSTRAINT [FKImpegnati]
GO
ALTER TABLE [dbo].[TURNI]  WITH CHECK ADD  CONSTRAINT [FKRiguardanti] FOREIGN KEY([IdSupermercato], [NumCorsia])
REFERENCES [dbo].[CORSIE] ([IdSupermercato], [NumCorsia])
GO
ALTER TABLE [dbo].[TURNI] CHECK CONSTRAINT [FKRiguardanti]
GO
ALTER TABLE [dbo].[CATEGORIE]  WITH NOCHECK ADD  CONSTRAINT [IDCATEGORIE_CHK] CHECK  (([dbo].[myFunctionCAT]([CATEGORIE].[IdCategoria])='True'))
GO
ALTER TABLE [dbo].[CATEGORIE] NOCHECK CONSTRAINT [IDCATEGORIE_CHK]
GO
ALTER TABLE [dbo].[FORNITURE]  WITH NOCHECK ADD  CONSTRAINT [IDFORNITURE_CHK] CHECK  (([dbo].[myFunctionFOR]([FORNITURE].[IdFornitura])='True'))
GO
ALTER TABLE [dbo].[FORNITURE] NOCHECK CONSTRAINT [IDFORNITURE_CHK]
GO
ALTER TABLE [dbo].[OFFERTE]  WITH NOCHECK ADD  CONSTRAINT [IDOFFERTE_CHK] CHECK  (([dbo].[myFunctionOFF]([OFFERTE].[IdOfferta])='True'))
GO
ALTER TABLE [dbo].[OFFERTE] NOCHECK CONSTRAINT [IDOFFERTE_CHK]
GO
ALTER TABLE [dbo].[PRODOTTI]  WITH NOCHECK ADD  CONSTRAINT [IDPRODOTTI_CHK] CHECK  (([dbo].[myFunctionPRO]([PRODOTTI].[IdProdotto])='True'))
GO
ALTER TABLE [dbo].[PRODOTTI] NOCHECK CONSTRAINT [IDPRODOTTI_CHK]
GO
ALTER TABLE [dbo].[PRODOTTI]  WITH NOCHECK ADD  CONSTRAINT [IDPRODOTTI_CHK2] CHECK  (([dbo].[myFunctionPROD]([IdProdotto])='True'))
GO
ALTER TABLE [dbo].[PRODOTTI] NOCHECK CONSTRAINT [IDPRODOTTI_CHK2]
GO
ALTER TABLE [dbo].[RIFORNITORI]  WITH NOCHECK ADD  CONSTRAINT [IDRIFORNITORI_CHK] CHECK  (([dbo].[myFunctionRIF]([RIFORNITORI].[IdRifornitore])='True'))
GO
ALTER TABLE [dbo].[RIFORNITORI] NOCHECK CONSTRAINT [IDRIFORNITORI_CHK]
GO
ALTER TABLE [dbo].[SCONTRINI]  WITH NOCHECK ADD  CONSTRAINT [IDSCONTRINI_CHK] CHECK  (([dbo].[myFunctionSCO]([SCONTRINI].[NumScontrino])='True'))
GO
ALTER TABLE [dbo].[SCONTRINI] NOCHECK CONSTRAINT [IDSCONTRINI_CHK]
GO
ALTER TABLE [dbo].[SUPERMERCATI]  WITH NOCHECK ADD  CONSTRAINT [IDSEDI_CHK] CHECK  (([dbo].[myFunctionSUP]([SUPERMERCATI].[IdSupermercato])='True'))
GO
ALTER TABLE [dbo].[SUPERMERCATI] NOCHECK CONSTRAINT [IDSEDI_CHK]
GO
