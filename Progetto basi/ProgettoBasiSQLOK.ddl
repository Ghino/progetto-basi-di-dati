-- *********************************************
-- * Standard SQL generation                   
-- *--------------------------------------------
-- * DB-MAIN version: 9.3.0              
-- * Generator date: Feb 16 2016              
-- * Generation date: Tue Jul 05 12:31:09 2016 
-- * LUN file: C:\Users\Filippo\Desktop\ProgettoBasi.lun 
-- * Schema: prova logica/1 
-- ********************************************* 


-- Database Section
-- ________________ 

create database prova logica;


-- DBSpace Section
-- _______________


-- Tables Section
-- _____________ 

create table ACCOUNT (
     Utente char(15) not null,
     Password char(15) not null,
     Nome char(15) not null,
     Cognome char(15) not null,
     CodFisc char(16) not null,
     NumTel numeric(15) not null,
     Direttore char not null,
     constraint IDACCOUNT_ID primary key (Utente));

create table CATEGORIE (
     IdCategoria numeric(5) not null,
     Nome char(15) not null,
     Descrizione char(50) not null,
     constraint IDCATEGORIE_ID primary key (IdCategoria));

create table Comprendono (
     IdFornitura numeric(5) not null,
     Quantit� numeric(5) not null,
     Prezzo numeric(5,2) not null,
     IdProdotto numeric(5) not null,
     constraint FKCom_FOR_ID primary key (IdFornitura));

create table CORSIE (
     IdSupermercato numeric(5) not null,
     NumCorsia numeric(2) not null,
     Capacit� numeric(5) not null,
     IdCategoria numeric(5) not null,
     constraint IDCORSIE SUPERMERCATO primary key (IdSupermercato, NumCorsia));

create table Di (
     IdProdotto numeric(5) not null,
     Data date not null,
     NumScontrino numeric(5) not null,
     Quantit� numeric(5) not null,
     constraint IDDi primary key (NumScontrino, IdProdotto, Data));

create table Forniscono (
     IdProdotto numeric(5) not null,
     IdRifornitore numeric(5) not null,
     constraint IDForniscono primary key (IdRifornitore, IdProdotto));

create table FORNITURE (
     IdFornitura numeric(5) not null,
     Data date not null,
     IdRifornitore numeric(5) not null,
     IdSupermercato numeric(5) not null,
     constraint IDFORNITURE_ID primary key (IdFornitura));

create table OFFERTE (
     IdOfferta numeric(5) not null,
     DataInzio date not null,
     DataFine date not null,
     Descrizione char(50) not null,
     PercentualeSconto numeric(3) not null,
     constraint IDOFFERTE_ID primary key (IdOfferta));

create table PREZZI ATTUALI (
     IdProdotto numeric(5) not null,
     Data date not null,
     Valore numeric(5,2) not null,
     constraint IDPREZZI ATTUALI primary key (IdProdotto, Data));

create table PRODOTTI (
     IdProdotto numeric(5) not null,
     Nome char(15) not null,
     Descrizione char(50) not null,
     PrezzoUnitario numeric(5,2) not null,
     Data d'entrata in commercio date not null,
     IdCategoria numeric(5) not null,
     IdOfferta numeric(5),
     constraint IDPRODOTTI_ID primary key (IdProdotto));

create table RIFORNITORI (
     IdRifornitore numeric(5) not null,
     Nome char(15) not null,
     Localit� char(15) not null,
     Ind_ViaPiazza char(15) not null,
     Ind_NumCivico numeric(5) not null,
     constraint IDRIFORNITORI_ID primary key (IdRifornitore));

create table SCONTRINI (
     NumScontrino numeric(5) not null,
     Data date not null,
     Totale numeric(5,2) not null,
     IdSupermercato numeric(5) not null,
     constraint IDSCONTRINI_ID primary key (NumScontrino));

create table SUPERMERCATI (
     IdSupermercato numeric(5) not null,
     Utente char(15) not null,
     Nome char(15) not null,
     Localit� char(15) not null,
     Ind_ViaPiazza char(15) not null,
     Ind_NumCivico numeric(5) not null,
     Guadagno netto numeric(5,2) not null,
     constraint IDSEDI_ID primary key (IdSupermercato),
     constraint FKGestiscono_ID unique (Utente));

create table TURNI (
     IdSupermercato numeric(5) not null,
     NumCorsia numeric(2) not null,
     OraInizio numeric(2,2) not null,
     OraFine numeric(2,2) not null,
     Utente char(15) not null,
     constraint IDTURNI primary key (IdSupermercato, NumCorsia, OraInizio, OraFine));

create table Tengono (
     IdSupermercato numeric(5) not null,
     NumCorsia numeric(2) not null,
     IdProdotto numeric(5) not null,
     Quantit� numeric(2) not null,
     constraint IDTengono primary key (IdSupermercato, NumCorsia, IdProdotto));


-- Constraints Section
-- ___________________ 

alter table ACCOUNT add constraint IDACCOUNT_CHK
     check(exists(select * from TURNI
                  where TURNI.Utente = Utente)); 

alter table ACCOUNT add constraint IDACCOUNT_CHK
     check(exists(select * from SUPERMERCATI
                  where SUPERMERCATI.Utente = Utente)); 

alter table CATEGORIE add constraint IDCATEGORIE_CHK
     check(exists(select * from PRODOTTI
                  where PRODOTTI.IdCategoria = IdCategoria)); 

alter table Comprendono add constraint FKCom_FOR
     foreign key (IdFornitura)
     references FORNITURE;

alter table Comprendono add constraint FKCom_PRO
     foreign key (IdProdotto)
     references PRODOTTI;

alter table CORSIE add constraint FKComposti da
     foreign key (IdSupermercato)
     references SUPERMERCATI;

alter table CORSIE add constraint FKContengono
     foreign key (IdCategoria)
     references CATEGORIE;

alter table Di add constraint FKDi_SCO
     foreign key (NumScontrino)
     references SCONTRINI;

alter table Di add constraint FKDi_PRE
     foreign key (IdProdotto, Data)
     references PREZZI ATTUALI;

alter table Forniscono add constraint FKFor_RIF
     foreign key (IdRifornitore)
     references RIFORNITORI;

alter table Forniscono add constraint FKFor_PRO
     foreign key (IdProdotto)
     references PRODOTTI;

alter table FORNITURE add constraint IDFORNITURE_CHK
     check(exists(select * from Comprendono
                  where Comprendono.IdFornitura = IdFornitura)); 

alter table FORNITURE add constraint FKProvvedono
     foreign key (IdRifornitore)
     references RIFORNITORI;

alter table FORNITURE add constraint FKOrdinano
     foreign key (IdSupermercato)
     references SUPERMERCATI;

alter table OFFERTE add constraint IDOFFERTE_CHK
     check(exists(select * from PRODOTTI
                  where PRODOTTI.IdOfferta = IdOfferta)); 

alter table PREZZI ATTUALI add constraint FKPresentano
     foreign key (IdProdotto)
     references PRODOTTI;

alter table PRODOTTI add constraint IDPRODOTTI_CHK
     check(exists(select * from PREZZI ATTUALI
                  where PREZZI ATTUALI.IdProdotto = IdProdotto)); 

alter table PRODOTTI add constraint IDPRODOTTI_CHK
     check(exists(select * from Forniscono
                  where Forniscono.IdProdotto = IdProdotto)); 

alter table PRODOTTI add constraint FKA cui appartengono
     foreign key (IdCategoria)
     references CATEGORIE;

alter table PRODOTTI add constraint FKRiguardano
     foreign key (IdOfferta)
     references OFFERTE;

alter table RIFORNITORI add constraint IDRIFORNITORI_CHK
     check(exists(select * from Forniscono
                  where Forniscono.IdRifornitore = IdRifornitore)); 

alter table SCONTRINI add constraint IDSCONTRINI_CHK
     check(exists(select * from Di
                  where Di.NumScontrino = NumScontrino)); 

alter table SCONTRINI add constraint FKRegistrano
     foreign key (IdSupermercato)
     references SUPERMERCATI;

alter table SUPERMERCATI add constraint IDSEDI_CHK
     check(exists(select * from CORSIE
                  where CORSIE.IdSupermercato = IdSupermercato)); 

alter table SUPERMERCATI add constraint FKGestiscono_FK
     foreign key (Utente)
     references ACCOUNT;

alter table TURNI add constraint FKRiguardanti
     foreign key (IdSupermercato, NumCorsia)
     references CORSIE;

alter table TURNI add constraint FKImpegnati
     foreign key (Utente)
     references ACCOUNT;

alter table Tengono add constraint FKTen_PRO
     foreign key (IdProdotto)
     references PRODOTTI;

alter table Tengono add constraint FKTen_COR
     foreign key (IdSupermercato, NumCorsia)
     references CORSIE;


-- Index Section
-- _____________ 

