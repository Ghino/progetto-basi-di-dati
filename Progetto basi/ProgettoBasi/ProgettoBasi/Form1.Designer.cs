﻿namespace ProgettoBasi
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.visualizzaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.supermercatiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prodottiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.scontriniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.turniToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.corsieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.iProdottiTenutiNelleVarieCorsieToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prezziAttualiDeiProdottiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incassoTotaleDiOgniSupermercatToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.guadagnoTotaleDiOgniSupermercatoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classificaDeiProdottiToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.doveLavoraUnUtenteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ilDirettoreDiUnSupermercatoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.prodottiAttualmenteInOffertaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.databaseGridView = new System.Windows.Forms.DataGridView();
            this.progettoBasiDataSetBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.inserisciUtente = new System.Windows.Forms.Panel();
            this.buttonOkUtente = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBoxUtente = new System.Windows.Forms.TextBox();
            this.inserisciSupermercato = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxSupermercato = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.inserimentoProdotto = new System.Windows.Forms.Panel();
            this.button2 = new System.Windows.Forms.Button();
            this.textBoxProdotto = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.inserisciIdSupermercato = new System.Windows.Forms.Panel();
            this.button3 = new System.Windows.Forms.Button();
            this.textBoxIdSupermercato = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.inserisciGiornoMeseAnno = new System.Windows.Forms.Panel();
            this.button4 = new System.Windows.Forms.Button();
            this.textBoxGiornoMeseAnno = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.inserimentoAcc = new System.Windows.Forms.Panel();
            this.button5 = new System.Windows.Forms.Button();
            this.textBoxAccNumTel = new System.Windows.Forms.TextBox();
            this.textBoxAccCF = new System.Windows.Forms.TextBox();
            this.textBoxAccCognome = new System.Windows.Forms.TextBox();
            this.textBoxAccNome = new System.Windows.Forms.TextBox();
            this.textBoxAccPassword = new System.Windows.Forms.TextBox();
            this.textBoxAccUtente = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.insDipendente = new System.Windows.Forms.Button();
            this.inserimentoFail = new System.Windows.Forms.Panel();
            this.errorMessage = new System.Windows.Forms.Label();
            this.button7 = new System.Windows.Forms.Button();
            this.insTurno = new System.Windows.Forms.Button();
            this.inserisciTurno = new System.Windows.Forms.Panel();
            this.button9 = new System.Windows.Forms.Button();
            this.textBoxTurnUtente = new System.Windows.Forms.TextBox();
            this.textBoxTurnOraFine = new System.Windows.Forms.TextBox();
            this.textBoxTurnOraInizio = new System.Windows.Forms.TextBox();
            this.textBoxTurnNumCorsia = new System.Windows.Forms.TextBox();
            this.textBoxTurnIdSupermercato = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.inserisciFornitura = new System.Windows.Forms.Panel();
            this.button6 = new System.Windows.Forms.Button();
            this.textBoxForIdFornitura = new System.Windows.Forms.TextBox();
            this.textBoxForQuantità = new System.Windows.Forms.TextBox();
            this.textBoxForIdProdotto = new System.Windows.Forms.TextBox();
            this.textBoxForIdSupermercato = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.insFornitura = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.databaseGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.progettoBasiDataSetBindingSource)).BeginInit();
            this.inserisciUtente.SuspendLayout();
            this.inserisciSupermercato.SuspendLayout();
            this.inserimentoProdotto.SuspendLayout();
            this.inserisciIdSupermercato.SuspendLayout();
            this.inserisciGiornoMeseAnno.SuspendLayout();
            this.inserimentoAcc.SuspendLayout();
            this.inserimentoFail.SuspendLayout();
            this.inserisciTurno.SuspendLayout();
            this.inserisciFornitura.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.visualizzaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(870, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // visualizzaToolStripMenuItem
            // 
            this.visualizzaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem1,
            this.supermercatiToolStripMenuItem,
            this.prodottiToolStripMenuItem,
            this.scontriniToolStripMenuItem,
            this.turniToolStripMenuItem,
            this.corsieToolStripMenuItem,
            this.iProdottiTenutiNelleVarieCorsieToolStripMenuItem,
            this.prezziAttualiDeiProdottiToolStripMenuItem,
            this.incassoTotaleDiOgniSupermercatToolStripMenuItem,
            this.guadagnoTotaleDiOgniSupermercatoToolStripMenuItem,
            this.elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem,
            this.classificaDeiProdottiToolStripMenuItem,
            this.classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem,
            this.doveLavoraUnUtenteToolStripMenuItem,
            this.ilDirettoreDiUnSupermercatoToolStripMenuItem,
            this.inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem,
            this.prodottiAttualmenteInOffertaToolStripMenuItem,
            this.fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem,
            this.incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem});
            this.visualizzaToolStripMenuItem.Name = "visualizzaToolStripMenuItem";
            this.visualizzaToolStripMenuItem.Size = new System.Drawing.Size(69, 20);
            this.visualizzaToolStripMenuItem.Text = "Visualizza";
            // 
            // toolStripMenuItem1
            // 
            this.toolStripMenuItem1.Name = "toolStripMenuItem1";
            this.toolStripMenuItem1.Size = new System.Drawing.Size(360, 22);
            this.toolStripMenuItem1.Text = "Account";
            this.toolStripMenuItem1.Click += new System.EventHandler(this.toolStripMenuItem1_Click);
            // 
            // supermercatiToolStripMenuItem
            // 
            this.supermercatiToolStripMenuItem.Name = "supermercatiToolStripMenuItem";
            this.supermercatiToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.supermercatiToolStripMenuItem.Text = "Supermercati";
            this.supermercatiToolStripMenuItem.Click += new System.EventHandler(this.supermercatiToolStripMenuItem_Click);
            // 
            // prodottiToolStripMenuItem
            // 
            this.prodottiToolStripMenuItem.Name = "prodottiToolStripMenuItem";
            this.prodottiToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.prodottiToolStripMenuItem.Text = "Prodotti";
            this.prodottiToolStripMenuItem.Click += new System.EventHandler(this.prodottiToolStripMenuItem_Click);
            // 
            // scontriniToolStripMenuItem
            // 
            this.scontriniToolStripMenuItem.Name = "scontriniToolStripMenuItem";
            this.scontriniToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.scontriniToolStripMenuItem.Text = "Scontrini";
            this.scontriniToolStripMenuItem.Click += new System.EventHandler(this.scontriniToolStripMenuItem_Click);
            // 
            // turniToolStripMenuItem
            // 
            this.turniToolStripMenuItem.Name = "turniToolStripMenuItem";
            this.turniToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.turniToolStripMenuItem.Text = "Turni";
            this.turniToolStripMenuItem.Click += new System.EventHandler(this.turniToolStripMenuItem_Click);
            // 
            // corsieToolStripMenuItem
            // 
            this.corsieToolStripMenuItem.Name = "corsieToolStripMenuItem";
            this.corsieToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.corsieToolStripMenuItem.Text = "Corsie";
            this.corsieToolStripMenuItem.Click += new System.EventHandler(this.corsieToolStripMenuItem_Click);
            // 
            // iProdottiTenutiNelleVarieCorsieToolStripMenuItem
            // 
            this.iProdottiTenutiNelleVarieCorsieToolStripMenuItem.Name = "iProdottiTenutiNelleVarieCorsieToolStripMenuItem";
            this.iProdottiTenutiNelleVarieCorsieToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.iProdottiTenutiNelleVarieCorsieToolStripMenuItem.Text = "I prodotti tenuti nelle varie corsie";
            this.iProdottiTenutiNelleVarieCorsieToolStripMenuItem.Click += new System.EventHandler(this.iProdottiTenutiNelleVarieCorsieToolStripMenuItem_Click);
            // 
            // prezziAttualiDeiProdottiToolStripMenuItem
            // 
            this.prezziAttualiDeiProdottiToolStripMenuItem.Name = "prezziAttualiDeiProdottiToolStripMenuItem";
            this.prezziAttualiDeiProdottiToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.prezziAttualiDeiProdottiToolStripMenuItem.Text = "Prezzi attuali dei prodotti";
            this.prezziAttualiDeiProdottiToolStripMenuItem.Click += new System.EventHandler(this.prezziAttualiDeiProdottiToolStripMenuItem_Click);
            // 
            // incassoTotaleDiOgniSupermercatToolStripMenuItem
            // 
            this.incassoTotaleDiOgniSupermercatToolStripMenuItem.Name = "incassoTotaleDiOgniSupermercatToolStripMenuItem";
            this.incassoTotaleDiOgniSupermercatToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.incassoTotaleDiOgniSupermercatToolStripMenuItem.Text = "Incasso totale di ogni supermercato";
            this.incassoTotaleDiOgniSupermercatToolStripMenuItem.Click += new System.EventHandler(this.incassoTotaleDiOgniSupermercatToolStripMenuItem_Click);
            // 
            // guadagnoTotaleDiOgniSupermercatoToolStripMenuItem
            // 
            this.guadagnoTotaleDiOgniSupermercatoToolStripMenuItem.Name = "guadagnoTotaleDiOgniSupermercatoToolStripMenuItem";
            this.guadagnoTotaleDiOgniSupermercatoToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.guadagnoTotaleDiOgniSupermercatoToolStripMenuItem.Text = "Guadagno totale di ogni supermercato";
            this.guadagnoTotaleDiOgniSupermercatoToolStripMenuItem.Click += new System.EventHandler(this.guadagnoTotaleDiOgniSupermercatoToolStripMenuItem_Click);
            // 
            // elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem
            // 
            this.elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem.Name = "elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem";
            this.elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem.Text = "Elenco dei prodotti forniti dai vari rifornitori";
            this.elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem.Click += new System.EventHandler(this.elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem_Click);
            // 
            // classificaDeiProdottiToolStripMenuItem
            // 
            this.classificaDeiProdottiToolStripMenuItem.Name = "classificaDeiProdottiToolStripMenuItem";
            this.classificaDeiProdottiToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.classificaDeiProdottiToolStripMenuItem.Text = "Classifica dei prodotti più venduti";
            this.classificaDeiProdottiToolStripMenuItem.Click += new System.EventHandler(this.classificaDeiProdottiToolStripMenuItem_Click);
            // 
            // classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem
            // 
            this.classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem.Name = "classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem";
            this.classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem.Text = "Classifica supermercati in base alla loro capacità totale";
            this.classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem.Click += new System.EventHandler(this.classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem_Click);
            // 
            // doveLavoraUnUtenteToolStripMenuItem
            // 
            this.doveLavoraUnUtenteToolStripMenuItem.Name = "doveLavoraUnUtenteToolStripMenuItem";
            this.doveLavoraUnUtenteToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.doveLavoraUnUtenteToolStripMenuItem.Text = "Dove lavora un utente";
            this.doveLavoraUnUtenteToolStripMenuItem.Click += new System.EventHandler(this.doveLavoraUnUtenteToolStripMenuItem_Click);
            // 
            // ilDirettoreDiUnSupermercatoToolStripMenuItem
            // 
            this.ilDirettoreDiUnSupermercatoToolStripMenuItem.Name = "ilDirettoreDiUnSupermercatoToolStripMenuItem";
            this.ilDirettoreDiUnSupermercatoToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.ilDirettoreDiUnSupermercatoToolStripMenuItem.Text = "Il direttore di un dato supermercato";
            this.ilDirettoreDiUnSupermercatoToolStripMenuItem.Click += new System.EventHandler(this.ilDirettoreDiUnSupermercatoToolStripMenuItem_Click);
            // 
            // inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem
            // 
            this.inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem.Name = "inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem";
            this.inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem.Text = "In quali supermercati si trova un dato prodotto";
            this.inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem.Click += new System.EventHandler(this.inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem_Click);
            // 
            // prodottiAttualmenteInOffertaToolStripMenuItem
            // 
            this.prodottiAttualmenteInOffertaToolStripMenuItem.Name = "prodottiAttualmenteInOffertaToolStripMenuItem";
            this.prodottiAttualmenteInOffertaToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.prodottiAttualmenteInOffertaToolStripMenuItem.Text = "Prodotti attualmente in offerta";
            this.prodottiAttualmenteInOffertaToolStripMenuItem.Click += new System.EventHandler(this.prodottiAttualmenteInOffertaToolStripMenuItem_Click);
            // 
            // fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem
            // 
            this.fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem.Name = "fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem";
            this.fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem.Text = "Forniture effettuate da un dato supermercato";
            this.fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem.Click += new System.EventHandler(this.fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem_Click);
            // 
            // incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem
            // 
            this.incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem.Name = "incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem";
            this.incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem.Size = new System.Drawing.Size(360, 22);
            this.incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem.Text = "Incasso totale dei supermercati in un dato giorno";
            this.incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem.Click += new System.EventHandler(this.incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem_Click);
            // 
            // databaseGridView
            // 
            this.databaseGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.databaseGridView.Location = new System.Drawing.Point(181, 27);
            this.databaseGridView.Name = "databaseGridView";
            this.databaseGridView.Size = new System.Drawing.Size(677, 421);
            this.databaseGridView.TabIndex = 1;
            // 
            // inserisciUtente
            // 
            this.inserisciUtente.Controls.Add(this.buttonOkUtente);
            this.inserisciUtente.Controls.Add(this.label1);
            this.inserisciUtente.Controls.Add(this.textBoxUtente);
            this.inserisciUtente.Location = new System.Drawing.Point(300, 141);
            this.inserisciUtente.Name = "inserisciUtente";
            this.inserisciUtente.Size = new System.Drawing.Size(200, 100);
            this.inserisciUtente.TabIndex = 2;
            this.inserisciUtente.Visible = false;
            // 
            // buttonOkUtente
            // 
            this.buttonOkUtente.Location = new System.Drawing.Point(131, 39);
            this.buttonOkUtente.Name = "buttonOkUtente";
            this.buttonOkUtente.Size = new System.Drawing.Size(49, 23);
            this.buttonOkUtente.TabIndex = 2;
            this.buttonOkUtente.Text = "Ok";
            this.buttonOkUtente.UseVisualStyleBackColor = true;
            this.buttonOkUtente.Click += new System.EventHandler(this.buttonOkUtente_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Utente:";
            // 
            // textBoxUtente
            // 
            this.textBoxUtente.Location = new System.Drawing.Point(12, 41);
            this.textBoxUtente.Name = "textBoxUtente";
            this.textBoxUtente.Size = new System.Drawing.Size(100, 20);
            this.textBoxUtente.TabIndex = 0;
            // 
            // inserisciSupermercato
            // 
            this.inserisciSupermercato.Controls.Add(this.button1);
            this.inserisciSupermercato.Controls.Add(this.textBoxSupermercato);
            this.inserisciSupermercato.Controls.Add(this.label2);
            this.inserisciSupermercato.Location = new System.Drawing.Point(297, 144);
            this.inserisciSupermercato.Name = "inserisciSupermercato";
            this.inserisciSupermercato.Size = new System.Drawing.Size(200, 100);
            this.inserisciSupermercato.TabIndex = 3;
            this.inserisciSupermercato.Visible = false;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(144, 44);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(29, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Ok";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxSupermercato
            // 
            this.textBoxSupermercato.Location = new System.Drawing.Point(18, 47);
            this.textBoxSupermercato.Name = "textBoxSupermercato";
            this.textBoxSupermercato.Size = new System.Drawing.Size(100, 20);
            this.textBoxSupermercato.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(15, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "IdSupermercato:";
            // 
            // inserimentoProdotto
            // 
            this.inserimentoProdotto.Controls.Add(this.button2);
            this.inserimentoProdotto.Controls.Add(this.textBoxProdotto);
            this.inserimentoProdotto.Controls.Add(this.label3);
            this.inserimentoProdotto.Location = new System.Drawing.Point(294, 147);
            this.inserimentoProdotto.Name = "inserimentoProdotto";
            this.inserimentoProdotto.Size = new System.Drawing.Size(200, 100);
            this.inserimentoProdotto.TabIndex = 4;
            this.inserimentoProdotto.Visible = false;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(147, 40);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(29, 23);
            this.button2.TabIndex = 2;
            this.button2.Text = "Ok";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // textBoxProdotto
            // 
            this.textBoxProdotto.Location = new System.Drawing.Point(18, 43);
            this.textBoxProdotto.Name = "textBoxProdotto";
            this.textBoxProdotto.Size = new System.Drawing.Size(100, 20);
            this.textBoxProdotto.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "IdProdotto:";
            // 
            // inserisciIdSupermercato
            // 
            this.inserisciIdSupermercato.Controls.Add(this.button3);
            this.inserisciIdSupermercato.Controls.Add(this.textBoxIdSupermercato);
            this.inserisciIdSupermercato.Controls.Add(this.label4);
            this.inserisciIdSupermercato.Location = new System.Drawing.Point(291, 150);
            this.inserisciIdSupermercato.Name = "inserisciIdSupermercato";
            this.inserisciIdSupermercato.Size = new System.Drawing.Size(200, 100);
            this.inserisciIdSupermercato.TabIndex = 5;
            this.inserisciIdSupermercato.Visible = false;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(150, 37);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(29, 23);
            this.button3.TabIndex = 2;
            this.button3.Text = "Ok";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // textBoxIdSupermercato
            // 
            this.textBoxIdSupermercato.Location = new System.Drawing.Point(27, 41);
            this.textBoxIdSupermercato.Name = "textBoxIdSupermercato";
            this.textBoxIdSupermercato.Size = new System.Drawing.Size(100, 20);
            this.textBoxIdSupermercato.TabIndex = 1;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(85, 13);
            this.label4.TabIndex = 0;
            this.label4.Text = "IdSupermercato:";
            // 
            // inserisciGiornoMeseAnno
            // 
            this.inserisciGiornoMeseAnno.Controls.Add(this.button4);
            this.inserisciGiornoMeseAnno.Controls.Add(this.textBoxGiornoMeseAnno);
            this.inserisciGiornoMeseAnno.Controls.Add(this.label5);
            this.inserisciGiornoMeseAnno.Location = new System.Drawing.Point(291, 150);
            this.inserisciGiornoMeseAnno.Name = "inserisciGiornoMeseAnno";
            this.inserisciGiornoMeseAnno.Size = new System.Drawing.Size(197, 100);
            this.inserisciGiornoMeseAnno.TabIndex = 6;
            this.inserisciGiornoMeseAnno.Visible = false;
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(150, 37);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(29, 23);
            this.button4.TabIndex = 2;
            this.button4.Text = "Ok";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // textBoxGiornoMeseAnno
            // 
            this.textBoxGiornoMeseAnno.Location = new System.Drawing.Point(24, 40);
            this.textBoxGiornoMeseAnno.Name = "textBoxGiornoMeseAnno";
            this.textBoxGiornoMeseAnno.Size = new System.Drawing.Size(110, 20);
            this.textBoxGiornoMeseAnno.TabIndex = 1;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(21, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(120, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "\"Giorno - Mese - Anno\":";
            // 
            // inserimentoAcc
            // 
            this.inserimentoAcc.Controls.Add(this.button5);
            this.inserimentoAcc.Controls.Add(this.textBoxAccNumTel);
            this.inserimentoAcc.Controls.Add(this.textBoxAccCF);
            this.inserimentoAcc.Controls.Add(this.textBoxAccCognome);
            this.inserimentoAcc.Controls.Add(this.textBoxAccNome);
            this.inserimentoAcc.Controls.Add(this.textBoxAccPassword);
            this.inserimentoAcc.Controls.Add(this.textBoxAccUtente);
            this.inserimentoAcc.Controls.Add(this.label11);
            this.inserimentoAcc.Controls.Add(this.label10);
            this.inserimentoAcc.Controls.Add(this.label9);
            this.inserimentoAcc.Controls.Add(this.label8);
            this.inserimentoAcc.Controls.Add(this.label7);
            this.inserimentoAcc.Controls.Add(this.label6);
            this.inserimentoAcc.Location = new System.Drawing.Point(195, 141);
            this.inserimentoAcc.Name = "inserimentoAcc";
            this.inserimentoAcc.Size = new System.Drawing.Size(649, 109);
            this.inserimentoAcc.TabIndex = 7;
            this.inserimentoAcc.Visible = false;
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(284, 67);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 33);
            this.button5.TabIndex = 12;
            this.button5.Text = "Ok";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBoxAccNumTel
            // 
            this.textBoxAccNumTel.Location = new System.Drawing.Point(542, 41);
            this.textBoxAccNumTel.Name = "textBoxAccNumTel";
            this.textBoxAccNumTel.Size = new System.Drawing.Size(94, 20);
            this.textBoxAccNumTel.TabIndex = 11;
            // 
            // textBoxAccCF
            // 
            this.textBoxAccCF.Location = new System.Drawing.Point(454, 41);
            this.textBoxAccCF.Name = "textBoxAccCF";
            this.textBoxAccCF.Size = new System.Drawing.Size(83, 20);
            this.textBoxAccCF.TabIndex = 10;
            // 
            // textBoxAccCognome
            // 
            this.textBoxAccCognome.Location = new System.Drawing.Point(342, 41);
            this.textBoxAccCognome.Name = "textBoxAccCognome";
            this.textBoxAccCognome.Size = new System.Drawing.Size(105, 20);
            this.textBoxAccCognome.TabIndex = 9;
            // 
            // textBoxAccNome
            // 
            this.textBoxAccNome.Location = new System.Drawing.Point(249, 41);
            this.textBoxAccNome.Name = "textBoxAccNome";
            this.textBoxAccNome.Size = new System.Drawing.Size(86, 20);
            this.textBoxAccNome.TabIndex = 8;
            // 
            // textBoxAccPassword
            // 
            this.textBoxAccPassword.Location = new System.Drawing.Point(123, 41);
            this.textBoxAccPassword.Name = "textBoxAccPassword";
            this.textBoxAccPassword.Size = new System.Drawing.Size(117, 20);
            this.textBoxAccPassword.TabIndex = 7;
            // 
            // textBoxAccUtente
            // 
            this.textBoxAccUtente.Location = new System.Drawing.Point(26, 41);
            this.textBoxAccUtente.Name = "textBoxAccUtente";
            this.textBoxAccUtente.Size = new System.Drawing.Size(88, 20);
            this.textBoxAccUtente.TabIndex = 6;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(539, 16);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(56, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Num.Tel. :";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(452, 16);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(32, 13);
            this.label10.TabIndex = 4;
            this.label10.Text = "C.F. :";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(340, 16);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "Cognome:";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(246, 16);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(38, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Nome:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(123, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Password:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(23, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(42, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Utente:";
            // 
            // insDipendente
            // 
            this.insDipendente.Location = new System.Drawing.Point(50, 47);
            this.insDipendente.Name = "insDipendente";
            this.insDipendente.Size = new System.Drawing.Size(75, 41);
            this.insDipendente.TabIndex = 8;
            this.insDipendente.Text = "Inserisci Dipendente";
            this.insDipendente.UseVisualStyleBackColor = true;
            this.insDipendente.Click += new System.EventHandler(this.button6_Click);
            // 
            // inserimentoFail
            // 
            this.inserimentoFail.Controls.Add(this.errorMessage);
            this.inserimentoFail.Controls.Add(this.button7);
            this.inserimentoFail.Location = new System.Drawing.Point(23, 348);
            this.inserimentoFail.Name = "inserimentoFail";
            this.inserimentoFail.Size = new System.Drawing.Size(138, 100);
            this.inserimentoFail.TabIndex = 9;
            this.inserimentoFail.Visible = false;
            // 
            // errorMessage
            // 
            this.errorMessage.AutoSize = true;
            this.errorMessage.Location = new System.Drawing.Point(24, 22);
            this.errorMessage.Name = "errorMessage";
            this.errorMessage.Size = new System.Drawing.Size(91, 13);
            this.errorMessage.TabIndex = 1;
            this.errorMessage.Text = "Inserimento Fallito";
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(36, 55);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(66, 23);
            this.button7.TabIndex = 0;
            this.button7.Text = "Ok";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // insTurno
            // 
            this.insTurno.Location = new System.Drawing.Point(50, 113);
            this.insTurno.Name = "insTurno";
            this.insTurno.Size = new System.Drawing.Size(75, 39);
            this.insTurno.TabIndex = 10;
            this.insTurno.Text = "Inserisci Turno";
            this.insTurno.UseVisualStyleBackColor = true;
            this.insTurno.Click += new System.EventHandler(this.button8_Click);
            // 
            // inserisciTurno
            // 
            this.inserisciTurno.Controls.Add(this.button9);
            this.inserisciTurno.Controls.Add(this.textBoxTurnUtente);
            this.inserisciTurno.Controls.Add(this.textBoxTurnOraFine);
            this.inserisciTurno.Controls.Add(this.textBoxTurnOraInizio);
            this.inserisciTurno.Controls.Add(this.textBoxTurnNumCorsia);
            this.inserisciTurno.Controls.Add(this.textBoxTurnIdSupermercato);
            this.inserisciTurno.Controls.Add(this.label17);
            this.inserisciTurno.Controls.Add(this.label16);
            this.inserisciTurno.Controls.Add(this.label15);
            this.inserisciTurno.Controls.Add(this.label14);
            this.inserisciTurno.Controls.Add(this.label13);
            this.inserisciTurno.Location = new System.Drawing.Point(192, 144);
            this.inserisciTurno.Name = "inserisciTurno";
            this.inserisciTurno.Size = new System.Drawing.Size(536, 119);
            this.inserisciTurno.TabIndex = 11;
            this.inserisciTurno.Visible = false;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(230, 77);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 10;
            this.button9.Text = "Ok";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // textBoxTurnUtente
            // 
            this.textBoxTurnUtente.Location = new System.Drawing.Point(413, 45);
            this.textBoxTurnUtente.Name = "textBoxTurnUtente";
            this.textBoxTurnUtente.Size = new System.Drawing.Size(87, 20);
            this.textBoxTurnUtente.TabIndex = 9;
            // 
            // textBoxTurnOraFine
            // 
            this.textBoxTurnOraFine.Location = new System.Drawing.Point(315, 45);
            this.textBoxTurnOraFine.Name = "textBoxTurnOraFine";
            this.textBoxTurnOraFine.Size = new System.Drawing.Size(93, 20);
            this.textBoxTurnOraFine.TabIndex = 8;
            // 
            // textBoxTurnOraInizio
            // 
            this.textBoxTurnOraInizio.Location = new System.Drawing.Point(208, 46);
            this.textBoxTurnOraInizio.Name = "textBoxTurnOraInizio";
            this.textBoxTurnOraInizio.Size = new System.Drawing.Size(97, 20);
            this.textBoxTurnOraInizio.TabIndex = 7;
            // 
            // textBoxTurnNumCorsia
            // 
            this.textBoxTurnNumCorsia.Location = new System.Drawing.Point(129, 46);
            this.textBoxTurnNumCorsia.Name = "textBoxTurnNumCorsia";
            this.textBoxTurnNumCorsia.Size = new System.Drawing.Size(69, 20);
            this.textBoxTurnNumCorsia.TabIndex = 6;
            // 
            // textBoxTurnIdSupermercato
            // 
            this.textBoxTurnIdSupermercato.Location = new System.Drawing.Point(32, 46);
            this.textBoxTurnIdSupermercato.Name = "textBoxTurnIdSupermercato";
            this.textBoxTurnIdSupermercato.Size = new System.Drawing.Size(88, 20);
            this.textBoxTurnIdSupermercato.TabIndex = 5;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(410, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 4;
            this.label17.Text = "Utente:";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(312, 29);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(62, 13);
            this.label16.TabIndex = 3;
            this.label16.Text = "OraFine (h):";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(205, 28);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(66, 13);
            this.label15.TabIndex = 2;
            this.label15.Text = "OraInizio (h):";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(126, 28);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(61, 13);
            this.label14.TabIndex = 1;
            this.label14.Text = "NumCorsia:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(29, 28);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(85, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "IdSupermercato:";
            // 
            // inserisciFornitura
            // 
            this.inserisciFornitura.Controls.Add(this.button6);
            this.inserisciFornitura.Controls.Add(this.textBoxForIdFornitura);
            this.inserisciFornitura.Controls.Add(this.textBoxForQuantità);
            this.inserisciFornitura.Controls.Add(this.textBoxForIdProdotto);
            this.inserisciFornitura.Controls.Add(this.textBoxForIdSupermercato);
            this.inserisciFornitura.Controls.Add(this.label20);
            this.inserisciFornitura.Controls.Add(this.label19);
            this.inserisciFornitura.Controls.Add(this.label18);
            this.inserisciFornitura.Controls.Add(this.label12);
            this.inserisciFornitura.Location = new System.Drawing.Point(187, 150);
            this.inserisciFornitura.Name = "inserisciFornitura";
            this.inserisciFornitura.Size = new System.Drawing.Size(456, 125);
            this.inserisciFornitura.TabIndex = 12;
            this.inserisciFornitura.Visible = false;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(188, 89);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 24);
            this.button6.TabIndex = 8;
            this.button6.Text = "Ok";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click_2);
            // 
            // textBoxForIdFornitura
            // 
            this.textBoxForIdFornitura.Location = new System.Drawing.Point(331, 53);
            this.textBoxForIdFornitura.Name = "textBoxForIdFornitura";
            this.textBoxForIdFornitura.Size = new System.Drawing.Size(83, 20);
            this.textBoxForIdFornitura.TabIndex = 7;
            // 
            // textBoxForQuantità
            // 
            this.textBoxForQuantità.Location = new System.Drawing.Point(249, 52);
            this.textBoxForQuantità.Name = "textBoxForQuantità";
            this.textBoxForQuantità.Size = new System.Drawing.Size(74, 20);
            this.textBoxForQuantità.TabIndex = 6;
            // 
            // textBoxForIdProdotto
            // 
            this.textBoxForIdProdotto.Location = new System.Drawing.Point(151, 53);
            this.textBoxForIdProdotto.Name = "textBoxForIdProdotto";
            this.textBoxForIdProdotto.Size = new System.Drawing.Size(89, 20);
            this.textBoxForIdProdotto.TabIndex = 5;
            // 
            // textBoxForIdSupermercato
            // 
            this.textBoxForIdSupermercato.Location = new System.Drawing.Point(39, 53);
            this.textBoxForIdSupermercato.Name = "textBoxForIdSupermercato";
            this.textBoxForIdSupermercato.Size = new System.Drawing.Size(100, 20);
            this.textBoxForIdSupermercato.TabIndex = 4;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(328, 26);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(60, 13);
            this.label20.TabIndex = 3;
            this.label20.Text = "IdFornitura:";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(246, 26);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(50, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "Quantità:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(148, 26);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(59, 13);
            this.label18.TabIndex = 1;
            this.label18.Text = "IdProdotto:";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(36, 26);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 13);
            this.label12.TabIndex = 0;
            this.label12.Text = "IdSupermercato:";
            // 
            // insFornitura
            // 
            this.insFornitura.Location = new System.Drawing.Point(50, 182);
            this.insFornitura.Name = "insFornitura";
            this.insFornitura.Size = new System.Drawing.Size(75, 39);
            this.insFornitura.TabIndex = 13;
            this.insFornitura.Text = "Ordina Fornitura";
            this.insFornitura.UseVisualStyleBackColor = true;
            this.insFornitura.Click += new System.EventHandler(this.button6_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(870, 460);
            this.Controls.Add(this.insFornitura);
            this.Controls.Add(this.inserisciFornitura);
            this.Controls.Add(this.inserisciTurno);
            this.Controls.Add(this.insTurno);
            this.Controls.Add(this.inserimentoFail);
            this.Controls.Add(this.insDipendente);
            this.Controls.Add(this.inserimentoAcc);
            this.Controls.Add(this.inserisciGiornoMeseAnno);
            this.Controls.Add(this.inserisciIdSupermercato);
            this.Controls.Add(this.inserimentoProdotto);
            this.Controls.Add(this.inserisciSupermercato);
            this.Controls.Add(this.inserisciUtente);
            this.Controls.Add(this.databaseGridView);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "ProgettoBasi";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.databaseGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.progettoBasiDataSetBindingSource)).EndInit();
            this.inserisciUtente.ResumeLayout(false);
            this.inserisciUtente.PerformLayout();
            this.inserisciSupermercato.ResumeLayout(false);
            this.inserisciSupermercato.PerformLayout();
            this.inserimentoProdotto.ResumeLayout(false);
            this.inserimentoProdotto.PerformLayout();
            this.inserisciIdSupermercato.ResumeLayout(false);
            this.inserisciIdSupermercato.PerformLayout();
            this.inserisciGiornoMeseAnno.ResumeLayout(false);
            this.inserisciGiornoMeseAnno.PerformLayout();
            this.inserimentoAcc.ResumeLayout(false);
            this.inserimentoAcc.PerformLayout();
            this.inserimentoFail.ResumeLayout(false);
            this.inserimentoFail.PerformLayout();
            this.inserisciTurno.ResumeLayout(false);
            this.inserisciTurno.PerformLayout();
            this.inserisciFornitura.ResumeLayout(false);
            this.inserisciFornitura.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem visualizzaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem1;
        private System.Windows.Forms.DataGridView databaseGridView;
        private System.Windows.Forms.BindingSource progettoBasiDataSetBindingSource;
        //private ProgettoBasiDataSet progettoBasiDataSet;
        private System.Windows.Forms.ToolStripMenuItem incassoTotaleDiOgniSupermercatToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem guadagnoTotaleDiOgniSupermercatoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classificaDeiProdottiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem doveLavoraUnUtenteToolStripMenuItem;
        private System.Windows.Forms.Panel inserisciUtente;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBoxUtente;
        private System.Windows.Forms.Button buttonOkUtente;
        private System.Windows.Forms.ToolStripMenuItem supermercatiToolStripMenuItem;
        private System.Windows.Forms.Panel inserisciSupermercato;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxSupermercato;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ToolStripMenuItem ilDirettoreDiUnSupermercatoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prodottiToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem;
        private System.Windows.Forms.Panel inserimentoProdotto;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textBoxProdotto;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem prodottiAttualmenteInOffertaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem;
        private System.Windows.Forms.Panel inserisciIdSupermercato;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox textBoxIdSupermercato;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ToolStripMenuItem scontriniToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem;
        private System.Windows.Forms.Panel inserisciGiornoMeseAnno;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.TextBox textBoxGiornoMeseAnno;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ToolStripMenuItem turniToolStripMenuItem;
        private System.Windows.Forms.Panel inserimentoAcc;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBoxAccNumTel;
        private System.Windows.Forms.TextBox textBoxAccCF;
        private System.Windows.Forms.TextBox textBoxAccCognome;
        private System.Windows.Forms.TextBox textBoxAccNome;
        private System.Windows.Forms.TextBox textBoxAccPassword;
        private System.Windows.Forms.TextBox textBoxAccUtente;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button insDipendente;
        private System.Windows.Forms.Panel inserimentoFail;
        private System.Windows.Forms.Label errorMessage;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button insTurno;
        private System.Windows.Forms.Panel inserisciTurno;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBoxTurnIdSupermercato;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox textBoxTurnNumCorsia;
        private System.Windows.Forms.TextBox textBoxTurnOraInizio;
        private System.Windows.Forms.TextBox textBoxTurnOraFine;
        private System.Windows.Forms.TextBox textBoxTurnUtente;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Panel inserisciFornitura;
        private System.Windows.Forms.Button insFornitura;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox textBoxForIdSupermercato;
        private System.Windows.Forms.TextBox textBoxForIdProdotto;
        private System.Windows.Forms.TextBox textBoxForQuantità;
        private System.Windows.Forms.TextBox textBoxForIdFornitura;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.ToolStripMenuItem iProdottiTenutiNelleVarieCorsieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem corsieToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem prezziAttualiDeiProdottiToolStripMenuItem;
    }
}

