﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using System.Reflection;

namespace ProgettoBasi
{
    public partial class Form1 : Form
    {
        DatabaseDataContext db = new DatabaseDataContext();
        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            Debug.Write(AppDomain.CurrentDomain.BaseDirectory);
            var queryResult = from a in db.ACCOUNTs
                              select a;
            databaseGridView.DataSource = queryResult;
        }

        private void incassoTotaleDiOgniSupermercatToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from sc in db.SCONTRINIs
                              join su in db.SUPERMERCATIs on sc.IdSupermercato equals su.IdSupermercato into su_join
                              from su in su_join.DefaultIfEmpty()
                              group new { sc, su } by new
                              {
                                  sc.IdSupermercato,
                                  su.Nome
                              } into g
                              select new
                              {
                                  IdSupermercato = g.Key.IdSupermercato,
                                  Nome = g.Key.Nome,
                                  IncassoTotale = 
                                  g.Sum(p => p.sc.Totale)
                              };
            databaseGridView.DataSource = queryResult;
                                
        }

        private void guadagnoTotaleDiOgniSupermercatoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from sup in db.SUPERMERCATIs
                              select new
                              {
                                  sup.IdSupermercato,
                                  sup.Nome,
                                  sup.Guadagno_netto
                              };
            databaseGridView.DataSource = queryResult;
        }

        private void elencoDeiProdottiFornitiDaiVariRifornitoriToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from f in db.Fornisconos
                              select new
                              {
                                  f.PRODOTTI.Nome,
                                  Column1 = f.RIFORNITORI.Nome
                              };
            databaseGridView.DataSource = queryResult;
        }

        private void classificaDeiProdottiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from d in db.Dis
                              from p in db.PRODOTTIs
                              where p.IdProdotto == d.IdProdotto
                              group new { p, d } by new
                              {
                                  p.Nome
                              } into g
                              orderby g.Max(p => p.d.Quantità) descending
                              select new
                              {
                                  g.Key.Nome,
                                  totale = g.Sum(p => p.d.Quantità)
                              };
            databaseGridView.DataSource = queryResult;
        }

        private void classificaSupermercatiInBaseAllaLoroCapacitàTotaleToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from c in db.CORSIEs
                              group new { c.SUPERMERCATI, c } by new
                              {
                                  c.SUPERMERCATI.Nome,
                                  c.Capacità
                              } into g
                              orderby g.Key.Capacità descending
                              select new
                              {
                                  g.Key.Nome,
                                  CapacitàTotale = g.Sum(p => p.c.Capacità)
                              };
            databaseGridView.DataSource = queryResult;
        }

        private void doveLavoraUnUtenteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inserisciUtente.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void buttonOkUtente_Click(object sender, EventArgs e)
        {
            inserisciUtente.Visible = false;
            visualizzaToolStripMenuItem.Enabled = true;
            insDipendente.Enabled = true;
            insTurno.Enabled = true;
            insFornitura.Enabled = true;
            String utente = textBoxUtente.Text;

            var queryResultTmp = from ACCOUNT in db.ACCOUNTs
                                 where ACCOUNT.Utente == utente
                                 select ACCOUNT.Direttore;
            if (!queryResultTmp.Any())
            {
                databaseGridView.DataSource = null;
            }
            if (queryResultTmp.FirstOrDefault().Equals('1'))
            {
                var queryResult = from s in db.SUPERMERCATIs
                                  where s.ACCOUNT.Utente == utente
                                  select new
                                  {
                                      s.ACCOUNT.Nome,
                                      s.ACCOUNT.Cognome,
                                      Nome_Supermercato = s.Nome
                                  };
                databaseGridView.DataSource = queryResult;
            }
            else
            {
                var queryResult = from t in db.TURNIs
                                  from s in db.SUPERMERCATIs
                                  where
                                    t.ACCOUNT.Utente == utente &&
                                    t.IdSupermercato == s.IdSupermercato
                                  select new
                                  {
                                      t.ACCOUNT.Nome,
                                      t.ACCOUNT.Cognome,
                                      Nome_Supermercato = s.Nome,
                                      t.NumCorsia
                                  };
                databaseGridView.DataSource = queryResult;
            }
        }

        private void supermercatiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from sup in db.SUPERMERCATIs
                              select sup;
            databaseGridView.DataSource = queryResult;
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
        }

        private void ilDirettoreDiUnSupermercatoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inserisciSupermercato.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            inserisciSupermercato.Visible = false;
            visualizzaToolStripMenuItem.Enabled = true;
            insDipendente.Enabled = true;
            insTurno.Enabled = true;
            insFornitura.Enabled = true;
            String idSupermercato = textBoxSupermercato.Text;
            int n;
            if (int.TryParse(idSupermercato, out n))
            {
                var queryResult = from s in db.SUPERMERCATIs
                                  where
                                    s.IdSupermercato == n
                                  select new
                                  {
                                      s.Nome,
                                      Column1 = s.ACCOUNT.Nome,
                                      s.ACCOUNT.Cognome
                                  };
                databaseGridView.DataSource = queryResult;
            }
            else
            {
                databaseGridView.DataSource = null;
            }
        }

        private void prodottiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from p in db.PRODOTTIs
                              select p;
            databaseGridView.DataSource = queryResult;
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
        }

        private void inQualiSupermercatiSiTrovaUnDatoProdottoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inserimentoProdotto.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            inserimentoProdotto.Visible = false;
            visualizzaToolStripMenuItem.Enabled = true;
            insDipendente.Enabled = true;
            insTurno.Enabled = true;
            insFornitura.Enabled = true;
            String idProdotto = textBoxProdotto.Text;
            int n;
            if (int.TryParse(idProdotto, out n))
            {
                var queryResult = from sup in db.SUPERMERCATIs
                                  from t in db.Tengonos
                                  where
                                    sup.IdSupermercato == t.IdSupermercato &&
                                    t.IdProdotto == n
                                  select new
                                  {
                                      IdSupermercato = sup.IdSupermercato,
                                      Utente = sup.Utente,
                                      Nome = sup.Nome,
                                      Località = sup.Località,
                                      Ind_ViaPiazza = sup.Ind_ViaPiazza,
                                      Ind_NumCivico = sup.Ind_NumCivico,
                                      Guadagno_netto = sup.Guadagno_netto,
                                      t.NumCorsia,
                                      t.Quantità
                                  };
                databaseGridView.DataSource = queryResult;
            }
            else
            {
                databaseGridView.DataSource = null;
            }
        }

        private void prodottiAttualmenteInOffertaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var currentDate = DateTime.Today;
            var queryResult = from p in db.PRODOTTIs
                              join o in db.OFFERTEs on new { IdOfferta = Convert.ToDecimal(p.IdOfferta) } equals new { IdOfferta = o.IdOfferta } into o_join
                              from o in o_join.DefaultIfEmpty()
                              where
                                currentDate >= o.DataInzio && currentDate <= o.DataFine
                              select new
                              {
                                  p.Nome,
                                  IdOfferta = o.IdOfferta,
                                  DataInzio = o.DataInzio,
                                  DataFine = o.DataFine,
                                  Descrizione = o.Descrizione,
                                  PercentualeSconto = o.PercentualeSconto
                              };
            databaseGridView.DataSource = queryResult;
        }

        private void fornitureEffettuateDaUnDatoSupermercatoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inserisciIdSupermercato.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            inserisciIdSupermercato.Visible = false;
            visualizzaToolStripMenuItem.Enabled = true;
            insDipendente.Enabled = true;
            insTurno.Enabled = true;
            insFornitura.Enabled = true;
            String idSupermercato = textBoxIdSupermercato.Text;
            int n;
            if (int.TryParse(idSupermercato, out n))
            {
                var queryResult = from FORNITURE in db.FORNITUREs
                                  where
                                    FORNITURE.IdSupermercato == n
                                  select new
                                  {
                                      IdFornitura = FORNITURE.IdFornitura,
                                      Data = FORNITURE.Data,
                                      IdSupermercato = FORNITURE.IdSupermercato,
                                      IdRifornitore = FORNITURE.IdRifornitore
                                  };
                databaseGridView.DataSource = queryResult;
            }
            else
            {
                databaseGridView.DataSource = null;
            }
        }

        private void scontriniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from sc in db.SCONTRINIs
                              select sc;
            databaseGridView.DataSource = queryResult;
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
        }

        private void incassoTotaleDeiSupermercatiInUnDatoGiornoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            inserisciGiornoMeseAnno.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            inserisciGiornoMeseAnno.Visible = false;
            visualizzaToolStripMenuItem.Enabled = true;
            insDipendente.Enabled = true;
            insTurno.Enabled = true;
            insFornitura.Enabled = true;
            String data = textBoxGiornoMeseAnno.Text;
            DateTime data1;
            if (DateTime.TryParse(data, out data1))
            {
                var queryResult = from sc in db.SCONTRINIs
                                  join su in db.SUPERMERCATIs on sc.IdSupermercato equals su.IdSupermercato into su_join
                                  from su in su_join.DefaultIfEmpty()
                                  where
                                    sc.Data == data1
                                  group new { sc, su } by new
                                  {
                                      sc.IdSupermercato,
                                      su.Nome
                                  } into g
                                  select new
                                  {
                                      IdSupermercato = g.Key.IdSupermercato,
                                      Nome = g.Key.Nome,
                                      IncassoTotale = g.Sum(p => p.sc.Totale)
                                  };
                databaseGridView.DataSource = queryResult;
            }
            else
            {
                databaseGridView.DataSource = null;
            }
        }

        private void turniToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from t in db.TURNIs
                              select t;
            databaseGridView.DataSource = queryResult;
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            inserimentoAcc.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            inserimentoAcc.Visible = false;
            String utente = textBoxAccUtente.Text;
            String password = textBoxAccPassword.Text;
            String nome = textBoxAccNome.Text;
            String cognome = textBoxAccCognome.Text;
            String cf = textBoxAccCF.Text;
            String numTel = textBoxAccNumTel.Text;
            int n;
            if (int.TryParse(numTel, out n))
            {
                ACCOUNT iACCOUNT = new ACCOUNT
                {
                    Utente = utente,
                    Password = password,
                    Nome = nome,
                    Cognome = cognome,
                    CodFisc = cf,
                    NumTel = n,
                    Direttore = '0'
                };
                try
                {
                    db.ACCOUNTs.InsertOnSubmit(iACCOUNT);
                    db.SubmitChanges();
                    visualizzaToolStripMenuItem.Enabled = true;
                    insDipendente.Enabled = true;
                    insTurno.Enabled = true;
                    insFornitura.Enabled = true;
                }
                catch (Exception)
                {
                    inserimentoFail.Visible = true;
                }
            }
            else
            {
                inserimentoFail.Visible = true;
                databaseGridView.DataSource = null;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            inserimentoFail.Visible = false;
            visualizzaToolStripMenuItem.Enabled = true;
            insDipendente.Enabled = true;
            insTurno.Enabled = true;
            insFornitura.Enabled = true;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            inserisciTurno.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            inserisciTurno.Visible = false;
            String idSup = textBoxTurnIdSupermercato.Text;
            String numCorsia = textBoxTurnNumCorsia.Text;
            String oraInizio = textBoxTurnOraInizio.Text;
            String oraFine = textBoxTurnOraFine.Text;
            String utente = textBoxTurnUtente.Text;
            int n;
            int n1;
            double n2;
            double n3;
            if (int.TryParse(idSup, out n))
            {
                if (int.TryParse(numCorsia, out n1))
                {
                    if (double.TryParse(oraInizio, out n2))
                    {
                        if (double.TryParse(oraFine, out n3))
                        {
                            var queryResult = from a in db.ACCOUNTs
                                              where a.Utente == utente
                                              select a;
                            if (queryResult.Any())
                            {
                                var queryResult5 = from cor in db.CORSIEs
                                                   where cor.IdSupermercato == n && cor.NumCorsia == n1
                                                   select cor;
                                if (!queryResult.Any())
                                {
                                    inserimentoFail.Visible = true;
                                    databaseGridView.DataSource = null;
                                }
                                else
                                {
                                    var queryResult1 = from TURNI in db.TURNIs
                                                       where
                                                         (TURNI.IdSupermercato == n &&
                                                         TURNI.NumCorsia == n1) &&
                                                         ((n2 > ((double)TURNI.OraInizio) && n2 < ((double)TURNI.OraFine)) ||
                                                         (n3 > ((double)TURNI.OraInizio) && n3 < ((double)TURNI.OraFine)) ||
                                                         ((double)TURNI.OraInizio > n2 && (double)TURNI.OraInizio < n3) ||
                                                         ((double)TURNI.OraFine > n2 && (double)TURNI.OraFine < n3))
                                                       select new
                                                       {
                                                           IdSupermercato = TURNI.IdSupermercato,
                                                           NumCorsia = TURNI.NumCorsia,
                                                           OraInizio = TURNI.OraInizio,
                                                           OraFine = TURNI.OraFine,
                                                           Utente = TURNI.Utente
                                                       };
                                    if (!queryResult1.Any())
                                    {
                                        var queryResult2 = from TURNI in db.TURNIs
                                                           where
                                                             (TURNI.Utente == utente) &&
                                                             ((n2 > ((double)TURNI.OraInizio) && n2 < ((double)TURNI.OraFine)) ||
                                                             (n3 > ((double)TURNI.OraInizio) && n3 < ((double)TURNI.OraFine)) ||
                                                             ((double)TURNI.OraInizio > n2 && (double)TURNI.OraInizio < n3) ||
                                                             ((double)TURNI.OraFine > n2 && (double)TURNI.OraFine < n3))
                                                           select new
                                                           {
                                                               IdSupermercato = TURNI.IdSupermercato,
                                                               NumCorsia = TURNI.NumCorsia,
                                                               OraInizio = TURNI.OraInizio,
                                                               OraFine = TURNI.OraFine,
                                                               Utente = TURNI.Utente
                                                           };
                                        if (!queryResult2.Any())
                                        {
                                            TURNI iTURNI = new TURNI
                                            {
                                                IdSupermercato = n,
                                                NumCorsia = n1,
                                                OraInizio = (decimal)n2,
                                                OraFine = (decimal)n3,
                                                Utente = utente
                                            };
                                            try
                                            {
                                                db.TURNIs.InsertOnSubmit(iTURNI);
                                                db.SubmitChanges();
                                                visualizzaToolStripMenuItem.Enabled = true;
                                                insDipendente.Enabled = true;
                                                insTurno.Enabled = true;
                                                insFornitura.Enabled = true;
                                            }
                                            catch (Exception)
                                            {
                                                inserimentoFail.Visible = true;
                                                databaseGridView.DataSource = null;
                                            }
                                        }
                                        else
                                        {
                                            Debug.Write("utente già impegnato");
                                            inserimentoFail.Visible = true;
                                            databaseGridView.DataSource = null;
                                        }
                                    }
                                    else
                                    {
                                        Debug.Write("Corsia già impegnata");
                                        inserimentoFail.Visible = true;
                                        databaseGridView.DataSource = null;
                                    }
                                }
                            }
                            else
                            {
                                Debug.Write("utente non presente");
                                inserimentoFail.Visible = true;
                                databaseGridView.DataSource = null;
                            }
                            
                        }
                        else
                        {
                            inserimentoFail.Visible = true;
                            databaseGridView.DataSource = null;
                        }
                    }
                    else
                    {
                        Debug.Write("Errore nella conversione dell'ora inizio");
                        inserimentoFail.Visible = true;
                        databaseGridView.DataSource = null;
                    }
                }
                else
                {
                    inserimentoFail.Visible = true;
                    databaseGridView.DataSource = null;
                }
            }
            else
            {
                inserimentoFail.Visible = true;
                databaseGridView.DataSource = null;
            }
        }

        private void button6_Click_1(object sender, EventArgs e)
        {
            inserisciFornitura.Visible = true;
            visualizzaToolStripMenuItem.Enabled = false;
            insDipendente.Enabled = false;
            insTurno.Enabled = false;
            insFornitura.Enabled = false;
        }

        private void button6_Click_2(object sender, EventArgs e)
        {
            inserisciFornitura.Visible = false;
            String idSupermercato = textBoxForIdSupermercato.Text;
            String idProdotto = textBoxForIdProdotto.Text;
            String quantità = textBoxForQuantità.Text;
            String idFornitura = textBoxForIdFornitura.Text;
            int n;
            int n1;
            int n2;
            int n3;
            if (int.TryParse(idSupermercato, out n))
            {
                if (int.TryParse(idProdotto, out n1))
                {
                    if (int.TryParse(quantità, out n2))
                    {
                        if (int.TryParse(idFornitura, out n3))
                        {
                            var queryResult = from sup in db.SUPERMERCATIs
                                              where sup.IdSupermercato == n
                                              select sup;
                            if (queryResult.Any())
                            {
                                var queryResult2 = from prod in db.PRODOTTIs
                                                   where prod.IdProdotto == n1
                                                   select prod;
                                if (queryResult2.Any())
                                {
                                    var queryResult3 = from Tengono in
                                                           (from Tengono in db.Tengonos
                                                            where
                                                              Tengono.IdSupermercato == 2 &&
                                                              Tengono.NumCorsia ==
                                                                ((from CORSIE in db.CORSIEs
                                                                  where
                                                                    CORSIE.IdCategoria ==
                                                                      ((from PRODOTTI in db.PRODOTTIs
                                                                        where
                                                                          PRODOTTI.IdProdotto == 5
                                                                        select new
                                                                        {
                                                                            PRODOTTI.IdCategoria
                                                                        }).First().IdCategoria) &&
                                                                    CORSIE.IdSupermercato == 2
                                                                  select new
                                                                  {
                                                                      CORSIE.NumCorsia
                                                                  }).First().NumCorsia)
                                                            select new
                                                            {
                                                                Tengono.Quantità,
                                                                Dummy = "x"
                                                            })
                                                       group Tengono by new { Tengono.Dummy } into g
                                                       select new
                                                       {
                                                           Column1 = (decimal?)g.Sum(p => p.Quantità)
                                                       };
                                    var queryResult4 = from CORSIE in db.CORSIEs
                                                       where
                                                         CORSIE.IdSupermercato == 2 &&
                                                         CORSIE.NumCorsia ==
                                                           ((from CORSIE0 in db.CORSIEs
                                                             where
                                                               CORSIE0.IdCategoria ==
                                                                 ((from PRODOTTI in db.PRODOTTIs
                                                                   where
                                                                     PRODOTTI.IdProdotto == 5
                                                                   select new
                                                                   {
                                                                       PRODOTTI.IdCategoria
                                                                   }).First().IdCategoria) &&
                                                               CORSIE0.IdSupermercato == 2
                                                             select new
                                                             {
                                                                 CORSIE0.NumCorsia
                                                             }).First().NumCorsia)
                                                       select new
                                                       {
                                                           CORSIE.Capacità
                                                       };
                                    if (queryResult3.FirstOrDefault().Column1 + n2 > queryResult4.FirstOrDefault().Capacità)
                                    {
                                        inserimentoFail.Visible = true;
                                        databaseGridView.DataSource = null;
                                    }
                                    else
                                    {
                                        var queryResult5 = from ten in db.Tengonos
                                                           where ten.IdSupermercato == n && ten.IdProdotto == n1
                                                           select ten;
                                        if (queryResult5.Any())
                                        {
                                            inserimentoFail.Visible = true;
                                            databaseGridView.DataSource = null;
                                        }
                                        else
                                        {
                                            FORNITURE iFORNITURE = new FORNITURE
                                            {
                                                IdFornitura = n3,
                                                Data = System.DateTime.Today,
                                                IdSupermercato = n,
                                                IdRifornitore =
                                                    ((from Forniscono in db.Fornisconos
                                                      where
                                                        Forniscono.IdProdotto == n1
                                                      select new
                                                      {
                                                          Forniscono.IdRifornitore
                                                      }).First().IdRifornitore)
                                            };
                                            try
                                            {
                                                db.FORNITUREs.InsertOnSubmit(iFORNITURE);
                                                db.SubmitChanges();
                                                Comprendono iComprendono = new Comprendono
                                                {
                                                    IdProdotto = n1,
                                                    IdFornitura = n3,
                                                    Quantità = n2,
                                                    Prezzo = ((
                                                        ((from PRODOTTI in db.PRODOTTIs
                                                          where
                                                           PRODOTTI.IdProdotto == n1
                                                          select new
                                                          {
                                                              PRODOTTI.PrezzoUnitario
                                                          }).First().PrezzoUnitario) / 2) * n2)
                                                };
                                                try
                                                {
                                                    db.Comprendonos.InsertOnSubmit(iComprendono);
                                                    db.SubmitChanges();
                                                    var querySUPERMERCATI = from SUPERMERCATI in db.SUPERMERCATIs
                                                                            where
                                                                              SUPERMERCATI.IdSupermercato == n
                                                                            select SUPERMERCATI;
                                                    foreach (var SUPERMERCATI in querySUPERMERCATI)
                                                    {
                                                        SUPERMERCATI.Guadagno_netto = (SUPERMERCATI.Guadagno_netto -
                                                            ((from Comprendono in db.Comprendonos
                                                              where
                                                               Comprendono.IdFornitura == n3
                                                              select new
                                                              {
                                                                  Comprendono.Prezzo
                                                              }).First().Prezzo));
                                                    }
                                                    try
                                                    {
                                                        db.SubmitChanges();
                                                        Tengono iTengono = new Tengono
                                                        {
                                                            IdSupermercato = n,
                                                            NumCorsia =
                                                                ((from CORSIE in db.CORSIEs
                                                                  where
                                                                    CORSIE.IdCategoria ==
                                                                      ((from PRODOTTI in db.PRODOTTIs
                                                                        where
                                                                          PRODOTTI.IdProdotto == n1
                                                                        select new
                                                                        {
                                                                            PRODOTTI.IdCategoria
                                                                        }).First().IdCategoria) &&
                                                                    CORSIE.IdSupermercato == n
                                                                  select new
                                                                  {
                                                                      CORSIE.NumCorsia
                                                                  }).First().NumCorsia),
                                                            IdProdotto = n1,
                                                            Quantità = n2
                                                        };
                                                        try
                                                        {
                                                            db.Tengonos.InsertOnSubmit(iTengono);
                                                            db.SubmitChanges();
                                                            visualizzaToolStripMenuItem.Enabled = true;
                                                            insDipendente.Enabled = true;
                                                            insTurno.Enabled = true;
                                                            insFornitura.Enabled = true;
                                                        }
                                                        catch (Exception)
                                                        {
                                                            inserimentoFail.Visible = true;
                                                            databaseGridView.DataSource = null;
                                                        }
                                                    }
                                                    catch (Exception)
                                                    {
                                                        inserimentoFail.Visible = true;
                                                        databaseGridView.DataSource = null;
                                                    }
                                                }
                                                catch (Exception)
                                                {
                                                    inserimentoFail.Visible = true;
                                                    databaseGridView.DataSource = null;
                                                }
                                            }
                                            catch (Exception)
                                            {
                                                inserimentoFail.Visible = true;
                                                databaseGridView.DataSource = null;
                                            }
                                        }
                                    }
                                }
                                else
                                {
                                    inserimentoFail.Visible = true;
                                    databaseGridView.DataSource = null;
                                }
                            }
                            else
                            {
                                inserimentoFail.Visible = true;
                                databaseGridView.DataSource = null;
                            }
                        }
                        else
                        {
                            inserimentoFail.Visible = true;
                            databaseGridView.DataSource = null;
                        }
                    }
                    else
                    {
                        inserimentoFail.Visible = true;
                        databaseGridView.DataSource = null;
                    }
                }
                else
                {
                    inserimentoFail.Visible = true;
                    databaseGridView.DataSource = null;
                }
            }
            else
            {
                inserimentoFail.Visible = true;
                databaseGridView.DataSource = null;
            }
        }

        private void iProdottiTenutiNelleVarieCorsieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from ten in db.Tengonos
                              select ten;
            databaseGridView.DataSource = queryResult;
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
        }

        private void corsieToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from cor in db.CORSIEs
                              select cor;
            databaseGridView.DataSource = queryResult;
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
            databaseGridView.Columns.Remove(databaseGridView.Columns[databaseGridView.ColumnCount - 1]);
        }

        private void prezziAttualiDeiProdottiToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var queryResult = from pre in db.PREZZI_ATTUALIs
                              from pro in db.PRODOTTIs
                              where pre.IdProdotto == pro.IdProdotto
                              select new
                              {
                                  pro.Nome,
                                  pre.Valore
                              };
            databaseGridView.DataSource = queryResult;
        }
    }
}
