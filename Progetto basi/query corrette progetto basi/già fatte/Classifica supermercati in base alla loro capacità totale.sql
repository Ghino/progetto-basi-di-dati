select s.Nome, sum(c.Capacitą) as CapacitąTotale
from SUPERMERCATI s, CORSIE c
where s.IdSupermercato = c.IdSupermercato
group by s.Nome, c.Capacitą
order by c.Capacitą desc